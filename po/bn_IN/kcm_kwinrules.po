# translation of kcmkwinrules.po to Bengali INDIA
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Runa Bhattacharjee <runab@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-13 02:16+0000\n"
"PO-Revision-Date: 2009-01-07 14:37+0530\n"
"Last-Translator: Runa Bhattacharjee <runab@redhat.com>\n"
"Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr ""

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "%1-র জন্য অ্যাপ্লিকেশন সংক্রান্ত বৈশিষ্ট্য"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "%1-র জন্য উইন্ডো সংক্রান্ত বৈশিষ্ট্য"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "কম গুরুত্বপূর্ণ"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "সুনিশ্চিত মিল"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "আংশিক মিল"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "রেগুলার এক্সপ্রেশন"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "প্রাথমিকরূপে প্রয়োগ করা হবে"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "অবিলম্বে প্রয়োগ করা হবে"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "স্মরণে রাখা হবে"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "প্রভাব সৃষ্টি করবে না"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "বলপূর্বক"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr ""

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "সাময়িকভাবে বলপূর্বক করা হবে"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: package/contents/ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr ""

#: package/contents/ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr ""

#: package/contents/ui/main.qml:60
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr ""

#: package/contents/ui/main.qml:61
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New...</interface> button below to add some"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Unselect All"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:87
#, kde-format
msgid "Save Rules"
msgstr ""

#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add New..."
msgstr "নতুন...(&N)"

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Import..."
msgstr ""

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Cancel Export"
msgstr ""

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
#| msgid "Edit..."
msgid "Export..."
msgstr "সম্পাদনা..."

#: package/contents/ui/main.qml:207
#, kde-format
msgid "Edit"
msgstr "সম্পাদনা"

#: package/contents/ui/main.qml:216
#, kde-format
msgid "Duplicate"
msgstr ""

#: package/contents/ui/main.qml:225
#, kde-format
msgid "Delete"
msgstr "মুছে ফেলুন"

#: package/contents/ui/main.qml:238
#, kde-format
msgid "Import Rules"
msgstr ""

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Export Rules"
msgstr ""

#: package/contents/ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr ""

#: package/contents/ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr ""

#: package/contents/ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/RulesEditor.qml:63
#, fuzzy, kde-format
#| msgid "&Detect Window Properties"
msgid "No window properties changed"
msgstr "উইন্ডোর বৈশিষ্ট্য সনাক্ত করা হবে (&D)"

#: package/contents/ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""

#: package/contents/ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Close"
msgstr "বন্ধ করার যোগ্য (&C)"

#: package/contents/ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add Property..."
msgstr "নতুন...(&N)"

#: package/contents/ui/RulesEditor.qml:98
#, fuzzy, kde-format
#| msgid "&Detect Window Properties"
msgid "Detect Window Properties"
msgstr "উইন্ডোর বৈশিষ্ট্য সনাক্ত করা হবে (&D)"

#: package/contents/ui/RulesEditor.qml:114
#: package/contents/ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr ""

#: package/contents/ui/RulesEditor.qml:115
#: package/contents/ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr ""

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr ""

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:60
#, fuzzy, kde-format
#| msgid "None"
msgid "No"
msgstr "শূণ্য"

#: package/contents/ui/RulesEditor.qml:278
#: package/contents/ui/ValueEditor.qml:168
#: package/contents/ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr ""

#: package/contents/ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr ""

#: package/contents/ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr ""

#: package/contents/ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr ""

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "%1-র মান"

#: rulesmodel.cpp:221
#, fuzzy, kde-format
#| msgid "Window settings for %1"
msgid "New window settings"
msgstr "%1-র জন্য উইন্ডো সংক্রান্ত বৈশিষ্ট্য"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:382
#, fuzzy, kde-format
#| msgid "De&scription:"
msgid "Description"
msgstr "বিবরণ: (&s)"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Window"
msgid "Window matching"
msgstr "উইন্ডো (&W)"

#: rulesmodel.cpp:390
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class (application)"
msgstr "উইন্ডোর শ্রেণী (অ্যাপ্লিকেশনের প্রকৃতি) (&c)"

#: rulesmodel.cpp:398
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Match whole window class"
msgstr "সম্পূর্ণ উইন্ডোর শ্রেণী মেলানো হবে (&h)"

#: rulesmodel.cpp:405
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Whole window class"
msgstr "সম্পূর্ণ উইন্ডোর শ্রেণী মেলানো হবে (&h)"

#: rulesmodel.cpp:411
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "Window types"
msgstr "উইন্ডোর প্রকৃতি: (&t)"

#: rulesmodel.cpp:419
#, fuzzy, kde-format
#| msgid "Window &role:"
msgid "Window role"
msgstr "উইন্ডোর ভূমিকা: (&r)"

#: rulesmodel.cpp:424
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Window title"
msgstr "উইন্ডোর শিরোনাম: (&i)"

#: rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Machine (hostname):"
msgid "Machine (hostname)"
msgstr "মেশিন (হোস্ট-নেম): (&M)"

#: rulesmodel.cpp:436
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Position"
msgstr "অবস্থান (&P)"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Size & Position"
msgstr "অবস্থান (&P)"

#: rulesmodel.cpp:442
#, fuzzy, kde-format
#| msgid "&Size"
msgid "Size"
msgstr "মাপ (&S)"

#: rulesmodel.cpp:448
#, fuzzy, kde-format
#| msgid "Maximized &horizontally"
msgid "Maximized horizontally"
msgstr "অনুভূমিক দিশায় সর্বোচ্চ মাপ (&h)"

#: rulesmodel.cpp:453
#, fuzzy, kde-format
#| msgid "Maximized &vertically"
msgid "Maximized vertically"
msgstr "উলম্ব দিশায় সর্বোচ্চ মাপ (&v)"

#: rulesmodel.cpp:461
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktop"
msgstr "সকল ডেস্কটপ"

#: rulesmodel.cpp:467
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktops"
msgstr "সকল ডেস্কটপ"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr ""

#: rulesmodel.cpp:502
#, fuzzy, kde-format
#| msgid "Splash Screen"
msgid "Screen"
msgstr "স্প্ল্যাশ স্ক্রিন"

#: rulesmodel.cpp:507
#, fuzzy, kde-format
#| msgid "&Fullscreen"
msgid "Fullscreen"
msgstr "সম্পূর্ণ পর্দায় প্রদর্শন (&F)"

#: rulesmodel.cpp:512
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Minimized"
msgstr "সর্বনিম্ন মাপ (&i)"

#: rulesmodel.cpp:517
#, fuzzy, kde-format
#| msgid "Sh&aded"
msgid "Shaded"
msgstr "ছায়াবৃত (&a)"

#: rulesmodel.cpp:522
#, fuzzy, kde-format
#| msgid "P&lacement"
msgid "Initial placement"
msgstr "স্থাপনা (&l)"

#: rulesmodel.cpp:531
#, fuzzy, kde-format
#| msgid "Ignore requested &geometry"
msgid "Ignore requested geometry"
msgstr "অনুরোধ করা জ্যামিতি উপেক্ষা করা হবে (&g)"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:546
#, fuzzy, kde-format
#| msgid "M&inimum size"
msgid "Minimum Size"
msgstr "সর্বনিম্ন মাপ (&i)"

#: rulesmodel.cpp:551
#, fuzzy, kde-format
#| msgid "M&aximum size"
msgid "Maximum Size"
msgstr "সর্বোচ্চ মাপ (&a)"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr ""

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr ""

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr ""

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr ""

#: rulesmodel.cpp:579
#, fuzzy, kde-format
#| msgid "Skip &taskbar"
msgid "Skip taskbar"
msgstr "টাস্ক-বার উপেক্ষা করা হবে (&t)"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:585
#, fuzzy, kde-format
#| msgid "Skip pa&ger"
msgid "Skip pager"
msgstr "পেজার উপেক্ষা করা হবে (&g)"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:591
#, fuzzy, kde-format
#| msgid "Skip pa&ger"
msgid "Skip switcher"
msgstr "পেজার উপেক্ষা করা হবে (&g)"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "শর্ট-কাট"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr ""

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr ""

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr ""

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr ""

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr ""

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr ""

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr ""

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr ""

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:686
#, fuzzy, kde-format
#| msgid "Block global shortcuts"
msgid "Ignore global shortcuts"
msgstr "সার্বজনীন শর্ট-কাট প্রতিরোধ করা হবে"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:698
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Closeable"
msgstr "বন্ধ করার যোগ্য (&C)"

#: rulesmodel.cpp:703
#, fuzzy, kde-format
#| msgid "Window &type"
msgid "Set window type"
msgstr "উইন্ডোর প্রকৃতি (&t)"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr ""

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr ""

#: rulesmodel.cpp:766
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class not available"
msgstr "উইন্ডোর শ্রেণী (অ্যাপ্লিকেশনের প্রকৃতি) (&c)"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:801
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "All Window Types"
msgstr "উইন্ডোর প্রকৃতি: (&t)"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "স্বাভাবিক উইন্ডো"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "ডায়লগ উইন্ডো"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr ""

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr ""

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "টুল-বার"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr ""

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "স্প্ল্যাশ স্ক্রিন"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "ডেস্কটপ"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "স্বতন্ত্র মেনু-বার"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr ""

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "সকল ডেস্কটপ"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr ""

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr ""

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr ""

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "ডিফল্ট মান"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr ""

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr ""

#: rulesmodel.cpp:869
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Maximized"
msgstr "সর্বনিম্ন মাপ (&i)"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "কেন্দ্রস্থিত"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "যথেচ্ছ"

#: rulesmodel.cpp:872
#, fuzzy, kde-format
#| msgid "Top-Left Corner"
msgid "In Top-Left Corner"
msgstr "উপরে-ডানদিকের কোণায়"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "মাউসের নীচে"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "প্রধান উইন্ডোর মধ্যে"

#: rulesmodel.cpp:881
#, fuzzy, kde-format
#| msgid "None"
msgid "None"
msgstr "শূণ্য"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "কম"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "স্বাভাবিক"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "বেশি"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr ""

#: rulesmodel.cpp:928
#, fuzzy, kde-format
#| msgid "On Main Window"
msgid "Unmanaged window"
msgstr "প্রধান উইন্ডোর মধ্যে"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#, fuzzy
#~| msgid "WId of the window for special window settings."
#~ msgid "KWin id of the window for special window settings."
#~ msgstr "উইন্ডো সংক্রান্ত বিশেষ বৈশিষ্ট্যের জন্য উইন্ডোর WId।"

#~ msgid "Whether the settings should affect all windows of the application."
#~ msgstr "অ্যাপ্লিকেশনের সকল উইন্ডোর জন্য এই মান প্রয়োগ করা হবে কি না।"

#~ msgid "This helper utility is not supposed to be called directly."
#~ msgstr "এই সহায়ক সামগ্রীটি সরাসরি প্রয়োগ করা সম্ভব হবে না।"

#, fuzzy
#~| msgid "Edit Window-Specific Settings"
#~ msgctxt "Window caption for the application wide rules dialog"
#~ msgid "Edit Application-Specific Settings"
#~ msgstr "উইন্ডোর জন্য সুনির্দিষ্ট বৈশিষ্ট্য সম্পাদনা করুন"

#~ msgid "Edit Window-Specific Settings"
#~ msgstr "উইন্ডোর জন্য সুনির্দিষ্ট বৈশিষ্ট্য সম্পাদনা করুন"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "রুণা ভট্টাচার্য্য"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "runab@redhat.com"

#, fuzzy
#~| msgid "Window &role:"
#~ msgid "Window Rules"
#~ msgstr "উইন্ডোর ভূমিকা: (&r)"

#, fuzzy
#~| msgid "Keep &above"
#~ msgid "Keep above"
#~ msgstr "উপরে স্থাপন করা হবে (&a)"

#, fuzzy
#~| msgid "Keep &below"
#~ msgid "Keep below"
#~ msgstr "নীচে স্থাপন করা হবে (&b)"

#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "KWin helper utility"
#~ msgstr "KWin সহায়ক সামগ্রী"

#, fuzzy
#~| msgid "&Detect Window Properties"
#~ msgid "Select properties"
#~ msgstr "উইন্ডোর বৈশিষ্ট্য সনাক্ত করা হবে (&D)"

#~ msgid "Override Type"
#~ msgstr "উপেক্ষা করার ধরন"

#~ msgid "Unknown - will be treated as Normal Window"
#~ msgstr "অজানা - স্বাভাবিক উইন্ডোরূপে ধার্য করা হবে"

#~ msgid "Information About Selected Window"
#~ msgstr "নির্বাচিত উইন্ডো সংক্রান্ত তথ্য"

#~ msgid "Class:"
#~ msgstr "শ্রেণী:"

#~ msgid "Role:"
#~ msgstr "ভূমিকা:"

#~ msgid "Type:"
#~ msgstr "প্রকৃতি:"

#~ msgid "Title:"
#~ msgstr "শিরোনাম:"

#~ msgid "Machine:"
#~ msgstr "মেশিন:"

#~ msgid "&Single Shortcut"
#~ msgstr "একটি শর্ট-কাট (&S)"

#~ msgid "C&lear"
#~ msgstr "পরিশ্রুত করুন (&l)"

#~ msgid "Window-Specific Settings Configuration Module"
#~ msgstr "উইন্ডোর জন্য সুনির্দিষ্ট বৈশিষ্ট্যের কনফিগারেশনের মডিউল"

#~ msgid "(c) 2004 KWin and KControl Authors"
#~ msgstr "(c) ২০০৪ KWin ও KControl-র নির্মাতাবৃন্দ"

#~ msgid "Lubos Lunak"
#~ msgstr "লুবোস লুনাক"

#~ msgid "Remember settings separately for every window"
#~ msgstr "প্রতিটি উইন্ডোর জন্য পৃথকভাবে মান স্মরণে রাখা হবে"

#~ msgid "Show internal settings for remembering"
#~ msgstr "স্মরণে রাখার জন্য ব্যবহারযোগ্য অভ্যন্তরীণ বৈশিষ্ট্য প্রদর্শন করা হবে"

#~ msgid "Internal setting for remembering"
#~ msgstr "স্মরণে রাখার জন্য ব্যবহারযোগ্য অভ্যন্তরীণ বৈশিষ্ট্য"

#~ msgid "&Modify..."
#~ msgstr "পরিবর্তন করুন (&M)"

#~ msgid "Move &Up"
#~ msgstr "উপরে স্থানান্তর (&U)"

#~ msgid "Move &Down"
#~ msgstr "নীচে স্থানান্তর (&D)"

#~ msgid "Unnamed entry"
#~ msgstr "নামবিহীন এনট্রি"

#~ msgid "Consult the documentation for more details."
#~ msgstr "অতিরিক্ত বিবরণের জন্য প্রাসঙ্গিক নথিপত্র পড়ুন।"

#~ msgid "Edit Shortcut"
#~ msgstr "শর্ট-কাট সম্পাদনা"

#~ msgid "0123456789-+,xX:"
#~ msgstr "0123456789-+,xX:"

#~ msgid "&Desktop"
#~ msgstr "ডেস্কটপ (&D)"

#~ msgid "kcmkwinrules"
#~ msgstr "kcmkwinrules"

#~ msgid "Transparent"
#~ msgstr "স্বচ্ছ"

#~ msgid "&Moving/resizing"
#~ msgstr "স্থানান্তর/মাপ পরিবর্তন (&M)"

#, fuzzy
#~| msgid "Title:"
#~ msgid "Tiled"
#~ msgstr "শিরোনাম:"

#~ msgid "Use window &class (whole application)"
#~ msgstr "উইন্ডোর শ্রেণী প্রয়োগ করা হবে (সমগ্র অ্যাপ্লিকেশন) (&c)"

#~ msgid "Use window class and window &role (specific window)"
#~ msgstr "উইন্ডোর শ্রেণী ও উইন্ডোর ভূমিকা প্রয়োগ করা হবে (সুনির্দিষ্ট উইন্ডো) (&r)"

#~ msgid "Use &whole window class (specific window)"
#~ msgstr "সম্পূর্ণ উইন্ডোর শ্রেণী প্রয়োগ করা হবে (সুনির্দিষ্ট উইন্ডো) (&w)"

#~ msgid "Match also window &title"
#~ msgstr "উইন্ডোর শিরোনামও মেলানো হবে (&t)"

#~ msgid "Extra role:"
#~ msgstr "অতিরিক্ত ভূমিকা:"

#~ msgid "Window &Extra"
#~ msgstr "উইন্ডোর অতিরিক্ত (&E)"

#~ msgid "&Geometry"
#~ msgstr "জ্যামিতি: (&G)"

#~ msgid "&Preferences"
#~ msgstr "পছন্দসই মান (&P)"

#~ msgid "&No border"
#~ msgstr "প্রান্ত বিহীন (&N)"

#~ msgid "0123456789"
#~ msgstr "0123456789"

#~ msgid "Strictly obey geometry"
#~ msgstr "কঠোরভাবে জ্যামিতি পালন করা হবে"
