# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# Martin Schlander <mschlander@opensuse.org>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:32+0000\n"
"PO-Revision-Date: 2020-02-08 15:54+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: package/contents/ui/Effect.qml:92
#, kde-format
msgid ""
"Author: %1\n"
"License: %2"
msgstr ""
"Ophavsmand: %1\n"
"Licens: %2"

#: package/contents/ui/Effect.qml:121
#, kde-format
msgctxt "@info:tooltip"
msgid "Show/Hide Video"
msgstr "Vis/skjul video"

#: package/contents/ui/Effect.qml:128
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure..."
msgstr "Indstil..."

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"Hint: To find out or configure how to activate an effect, look at the "
"effect's settings."
msgstr ""
"Tip: For at finde ud af, eller konfigurere, hvordan en effekt aktiveres, så "
"se på effektens indstillinger."

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Configure Filter"
msgstr "Indstil filter"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Exclude unsupported effects"
msgstr "Udelad ikke-understøttede effekter"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Exclude internal effects"
msgstr "Udelad interne effekter"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Get New Desktop Effects..."
msgstr "Hent nye skrivebordseffekter..."

#~ msgid "This module lets you configure desktop effects."
#~ msgstr "Med dette modul kan du indstille skrivebordseffekter."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "Desktop Effects"
#~ msgstr "Skrivebordseffekter"

#~ msgid "Vlad Zahorodnii"
#~ msgstr "Vlad Zahorodnii"

#~ msgid "Download New Desktop Effects"
#~ msgstr "Download nye skriveeffekter"
